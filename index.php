<?php
require_once('animal.php');
require_once('Frog.php');
require_once('Ape.php');


$sheep = new Animal("shaun");
echo "Nama Hewan : $sheep->name <br>";
echo "Jumlah Kaki :$sheep->legs <br>";
echo "Hewan Berdarah Dingin: $sheep->cold_blooded <br><br>";

$kodok = new Frog("buduk");
echo "Nama Hewan : $kodok->name <br>";
echo "Jumlah Kaki :$kodok->legs <br>";
echo "Berdarah: $kodok->cold_blooded <br>";
echo $kodok->jump(). "<br><br>";

$sungokong = new Ape("kera sakti");
echo "Nama Hewan : $sungokong->name <br>";
echo "Jumlah Kaki :$sungokong->legs <br>";
echo "Berdarah: $sungokong->cold_blooded <br>";
echo $sungokong->yell()


?>